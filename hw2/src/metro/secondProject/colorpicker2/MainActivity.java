package metro.secondProject.colorpicker2;

import java.util.Map;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.NumberPicker;

public class MainActivity extends Activity {

	private String colorName;
	private NumberPicker numPickerRed;
	private NumberPicker numPickerGreen;
	private NumberPicker numPickerBlue;
	private SurfaceView	surfaceView;
	private int colorRGBNumber;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		//Start of my code
		
		numPickerRed = (NumberPicker) findViewById(R.id.numberPickeRed);
		numPickerRed.setMaxValue(255);
		numPickerRed.setMinValue(0);
		numPickerGreen = (NumberPicker) findViewById(R.id.numberPickerGreen);
		numPickerGreen.setMaxValue(255);
		numPickerGreen.setMinValue(0);
		numPickerBlue = (NumberPicker) findViewById(R.id.numberPickerBlue);
		numPickerBlue.setMaxValue(255);
		numPickerBlue.setMinValue(0);
		surfaceView = (SurfaceView) findViewById(R.id.surfaceView1);



		
		numPickerRed.setOnValueChangedListener(new NumberPicker.
				OnValueChangeListener(){
			@Override 
			public void onValueChange(NumberPicker picker, int oldVal,
					int newVal){
				colorRGBNumber =Color.rgb(numPickerRed.getValue(),
						numPickerGreen.getValue(),numPickerBlue.getValue());
				surfaceView.setBackgroundColor(colorRGBNumber);
			}
		});
		
		numPickerGreen.setOnValueChangedListener(new NumberPicker.
				OnValueChangeListener(){
			@Override 
			public void onValueChange(NumberPicker picker, int oldVal,
					int newVal){
				colorRGBNumber =Color.rgb(numPickerRed.getValue(),
						numPickerGreen.getValue(),numPickerBlue.getValue());
				surfaceView.setBackgroundColor(colorRGBNumber);
			}
		});

		numPickerBlue.setOnValueChangedListener(new NumberPicker.
				OnValueChangeListener(){
			@Override 
			public void onValueChange(NumberPicker picker, int oldVal,
					int newVal){
				colorRGBNumber =Color.rgb(numPickerRed.getValue(),
						numPickerGreen.getValue(),numPickerBlue.getValue());
				surfaceView.setBackgroundColor(colorRGBNumber);
			}
		});
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_save:
			openSave();
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			return true;
		case R.id.action_recall:
			openRecall();
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void openSave(){

		final SharedPreferences shardPrefs = PreferenceManager.
				getDefaultSharedPreferences(this);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final EditText userInput = new EditText(this);
		userInput.setMaxLines(1);
		builder
			.setTitle("Enter the Name of the Color")
			.setView(userInput)
			.setCancelable(false)
			.setPositiveButton("SAVE",
			  new DialogInterface.OnClickListener() {
		    	Editor editor = shardPrefs.edit();
			    public void onClick(DialogInterface dialog,int id) {
			    	colorName =userInput.getText().toString();
			    	colorName.replaceAll("\\s+","");
			    	editor.putInt(colorName, colorRGBNumber);
			    	editor.commit();
		    }
		  });
		
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}
	
	public void openRecall(){
		
		final SharedPreferences shardPrefs = PreferenceManager.
				getDefaultSharedPreferences(this);
		Map<String, ?> shardPrefsMap = shardPrefs.getAll();
		final CharSequence[] colorSequence = shardPrefsMap.keySet().
				toArray(new CharSequence[shardPrefsMap.size()]);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder
			.setTitle("Pick a color")
			.setItems(colorSequence, new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
			    	colorRGBNumber = shardPrefs.
			    	getInt((String) colorSequence[id], 0);
			    	numPickerRed.setValue(Color.red(colorRGBNumber));
			    	numPickerGreen.setValue(Color.green(colorRGBNumber));
			    	numPickerBlue.setValue(Color.blue(colorRGBNumber));
			    	surfaceView.setBackgroundColor(colorRGBNumber);
			    }
		  } );
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}
	

	
}

