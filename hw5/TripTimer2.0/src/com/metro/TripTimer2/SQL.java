package com.metro.TripTimer2;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQL {

	public static final String KEY_START_TIME = "start_time";
	public static final String KEY_LOCATOIN = "location";
	public static final String KEY_ROUTE = "route";
	public static final String KEY_DURATION_TIME = "duration";

	private static final String DATABASE_NAME = "trip_time";
	private static final String DATABASE_TABLE = "trip_data";
	private static final int DATABASE_VERSION = 1;

	private DBHelper dbHelper;
	private final Context ourContext;
	private SQLiteDatabase sqliteDatabase;

	public SQL(Context context) {
		ourContext = context;
	}

	public SQL open() {
		dbHelper = new DBHelper(ourContext);
		sqliteDatabase = dbHelper.getWritableDatabase();
		return this;
	}

	public List<String> getAllData() {
		DateFormat dateFormat = DateFormat.getDateTimeInstance(
				DateFormat.DEFAULT, DateFormat.SHORT, Locale.US);
		String[] columns = new String[] { KEY_START_TIME, KEY_LOCATOIN,
				KEY_ROUTE, KEY_DURATION_TIME };
		Cursor cursor = sqliteDatabase.query(DATABASE_TABLE, columns, null,
				null, null, null, null);
		List<String> result = new ArrayList<String>();
		String dataRow = "";
		int iStartTime = cursor.getColumnIndex(KEY_START_TIME);
		int iLocation = cursor.getColumnIndex(KEY_LOCATOIN);
		int iRoute = cursor.getColumnIndex(KEY_ROUTE);
		int iDuratoin = cursor.getColumnIndex(KEY_DURATION_TIME);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			dataRow = dateFormat.format(new Date(cursor.getLong(iStartTime)))
					+ "    "
					+ cursor.getString(iLocation)
					+ "    "
					+ cursor.getString(iRoute)
					+ "    "
					+ String.format(
							"%02d:%02d.%02d",
							((cursor.getLong(iDuratoin) / (1000 * 60 * 60)) % 24),
							((cursor.getLong(iDuratoin) / (1000 * 60)) % 60),
							(cursor.getLong(iDuratoin) / 1000) % 60);

			result.add(dataRow);
			cursor.moveToNext();
		}

		return result;
	}

	public List<String> getlocatoinData(String locationName) {
		DateFormat dateFormat = DateFormat.getDateTimeInstance(
				DateFormat.DEFAULT, DateFormat.SHORT, Locale.US);
		String[] columns = new String[] { KEY_START_TIME, KEY_ROUTE,
				KEY_DURATION_TIME };

		Cursor cursor = sqliteDatabase.query(DATABASE_TABLE, columns,
				KEY_LOCATOIN + " = ? ", new String[] { locationName }, null,
				null, null);
		List<String> results = new ArrayList<String>();
		String dataRow = "";

		int iStartTime = cursor.getColumnIndex(KEY_START_TIME);
		int iRoute = cursor.getColumnIndex(KEY_ROUTE);
		int iDuratoin = cursor.getColumnIndex(KEY_DURATION_TIME);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			dataRow = dateFormat.format(new Date(cursor.getLong(iStartTime)))
					+ "    "
					+ cursor.getString(iRoute)
					+ "    "
					+ String.format(
							"%02d:%02d.%02d",
							((cursor.getLong(iDuratoin) / (1000 * 60 * 60)) % 24),
							((cursor.getLong(iDuratoin) / (1000 * 60)) % 60),
							(cursor.getLong(iDuratoin) / 1000) % 60);

			results.add(dataRow);
			cursor.moveToNext();
		}

		return results;
	}

	public long createEntry(long startTime, String locationName,
			String routeName, long duration) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(KEY_START_TIME, startTime);
		contentValues.put(KEY_LOCATOIN, locationName);
		contentValues.put(KEY_ROUTE, routeName);
		contentValues.put(KEY_DURATION_TIME, duration);

		return sqliteDatabase.insert(DATABASE_TABLE, null, contentValues);
	}

	public void close() {
		dbHelper.close();
	}

	private static class DBHelper extends SQLiteOpenHelper {

		public DBHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE " + DATABASE_TABLE + " (" + KEY_START_TIME
					+ " LONG NOT NULL, " + KEY_LOCATOIN + " TEXT NOT NULL, "
					+ KEY_ROUTE + " TEXT NOT NULL, " + KEY_DURATION_TIME
					+ " LONG NOT NULL)");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
			onCreate(db);

		}

	}

}
