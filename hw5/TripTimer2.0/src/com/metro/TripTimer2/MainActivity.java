package com.metro.TripTimer2;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity implements
		ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

	private GoogleMap googleMap;
	private Location location = null;
	private Location destination = null;
	private LocationClient locationClient;
	private SharedPreferences shardPrefsLocation;
	private long startTime, arrivalTime;
	private String destinationName, routName;
	private boolean getDialog = true;

	private static final LocationRequest REQUEST = LocationRequest.create()
			.setInterval(12).setFastestInterval(12)
			.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setUpMapIfNeeded();
		setUpLocationClientIfNeeded();
		locationClient.connect();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.actionLoadMarker:
			loadMarker();
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			return true;
		case R.id.canelTimer:
			cancelTimer();
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		setUpLocationClientIfNeeded();
		locationClient.connect();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (locationClient != null) {
			locationClient.disconnect();
		}
	}

	private void setUpLocationClientIfNeeded() {
		if (locationClient == null) {
			locationClient = new LocationClient(getApplicationContext(), this,
					this);
		}
	}

	private void setUpMapIfNeeded() {
		if (googleMap == null) {
			googleMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			if (googleMap != null) {
				googleMap.setMyLocationEnabled(true);
			}
		}

		googleMap.setOnMapLongClickListener(new OnMapLongClickListener() {
			@Override
			public void onMapLongClick(LatLng point) {
				googleMap.addMarker(new MarkerOptions()
						.position(point)
						.title(getAddress(point).getAddressLine(2))
						.snippet(
								getAddress(point).getAddressLine(0) + "\n"
										+ getAddress(point).getAddressLine(1)));
			}
		});

		googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
			@Override
			public void onInfoWindowClick(Marker marker) {
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
						new LatLng(marker.getPosition().latitude, 
								marker.getPosition().longitude-0.014), 15);
				googleMap.animateCamera(cameraUpdate);
				controlDialogs(marker);
			}
		});

		googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(Marker marker) {
				marker.showInfoWindow();
				return true;
			}
		});

		googleMap.setInfoWindowAdapter(new InfoWindowAdapter() {
			@Override
			public View getInfoWindow(Marker marker) {
				return null;
			}

			@Override
			public View getInfoContents(Marker marker) {
				View v = getLayoutInflater().inflate(
						R.layout.layout_info_window, null);
				TextView title = (TextView) v.findViewById(R.id.locatoinName1);
				title.setText(marker.getTitle() + "\t\t〉");
				return v;
			}
		});
	}

	public Address getAddress(LatLng addressLocation) {
		Double latitude, longitude;
		try {
			Geocoder geocoder = new Geocoder(this, Locale.getDefault());
			List<Address> address;
			latitude = addressLocation.latitude;
			longitude = addressLocation.longitude;
			if (latitude != 0 || longitude != 0) {
				address = geocoder.getFromLocation(latitude, longitude, 1);
				return address.get(0);
			} else {
				Toast.makeText(this, "latitude and longitude are null",
						Toast.LENGTH_LONG).show();
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void saveMarker(final Marker marker) {
		shardPrefsLocation = PreferenceManager
				.getDefaultSharedPreferences(this);
		AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
		final EditText userInput = new EditText(this);
		userInput.setMaxLines(1);
		builder.setTitle("Enter the Name of the Location")
				.setView(userInput)
				.setPositiveButton("SAVE",
						new DialogInterface.OnClickListener() {
							String markerName;
							Editor editor = shardPrefsLocation.edit();
							String latLngValueString = Double.toString(marker
									.getPosition().latitude)
									+ " "
									+ Double.toString(marker.getPosition().longitude);

							public void onClick(DialogInterface dialog, int id) {
								markerName = userInput.getText().toString();
								markerName.replaceAll("\\s+", "");
								editor.putString(markerName, latLngValueString);
								editor.commit();
								marker.setTitle(markerName);
								marker.showInfoWindow();
								((Dialog) dialog)
										.getWindow()
										.setSoftInputMode(
												WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();
								getDialog = false;
							}
						}).setOnDismissListener(new OnDismissListener() {

					@Override
					public void onDismiss(DialogInterface arg0) {
						controlDialogs(marker);
					}

				});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	public void controlDialogs(final Marker marker) {
		if (shardPrefsLocation == null
				|| !shardPrefsLocation.contains(marker.getTitle()) && getDialog) {
			saveMarker(marker);
		} else if (getDialog) {
			showPopupWindow(marker);
		}
		getDialog = true;
	}

	public void showPopupWindow(final Marker marker) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
		final TextView address = new TextView(this);
		address.setText(marker.getSnippet());
		builder.setTitle(marker.getTitle())
				.setMessage(address.getText())
				.setNeutralButton("Log", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						openlocatoinDialog(marker);
					}

				})
				.setPositiveButton("Start Timer",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								destinationName = marker.getTitle();
								startTimer(marker);
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();

							}

						});
		AlertDialog alertDialog = builder.create();
		WindowManager.LayoutParams wmlp = alertDialog.getWindow().getAttributes();//(Gravity.CENTER_HORIZONTAL&Gravity.LEFT);
		wmlp.gravity = Gravity.CENTER_VERTICAL&Gravity.LEFT | Gravity.CENTER_HORIZONTAL&Gravity.LEFT;
		alertDialog.show();
	}

	public void loadMarker() {

		shardPrefsLocation = PreferenceManager
				.getDefaultSharedPreferences(this);
		Map<String, ?> shardPrefsMap = shardPrefsLocation.getAll();
		final CharSequence[] markerSequence = shardPrefsMap.keySet().toArray(
				new CharSequence[shardPrefsMap.size()]);

		AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
		builder.setTitle("Pick a saved marker").setItems(markerSequence,
				new DialogInterface.OnClickListener() {
					double latitude = 0.0, longitude = 0.0;
					LatLng restoreMarker = null;

					public void onClick(DialogInterface dialog, int id) {
						String latLngValueString = shardPrefsLocation
								.getString((String) markerSequence[id],
										"nothing has been returned");
						String[] latLngValueArray = (String[]) latLngValueString
								.split(" ");
						latitude = Double.parseDouble(latLngValueArray[0]);
						longitude = Double.parseDouble(latLngValueArray[1]);
						restoreMarker = new LatLng(latitude, longitude);
						googleMap
								.addMarker(
										new MarkerOptions()
												.position(restoreMarker)
												.title((String) markerSequence[id])
												.snippet(
														getAddress(
																restoreMarker)
																.getAddressLine(
																		0)
																+ "\n"
																+ getAddress(
																		restoreMarker)
																		.getAddressLine(
																				1)))
								.showInfoWindow();
						CameraUpdate cameraUpdate = CameraUpdateFactory
								.newLatLngZoom(restoreMarker, 14);
						googleMap.animateCamera(cameraUpdate);
					}
				});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	public void setRouteName() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
		final EditText userInput = new EditText(this);
		userInput.setMaxLines(1);
		builder.setTitle("Enter the Route Name")
				.setView(userInput)
				.setCancelable(false)
				.setPositiveButton("SAVE",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								routName = userInput.getText().toString();
								Long duration = arrivalTime - startTime;
								setSQLData(startTime, destinationName,
										routName, duration);
								destination = null;
								startTime = 0;
								destinationName = null;
								arrivalTime = 0;
							}
						});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	public void openlocatoinDialog(Marker marker) {
		final CharSequence[] colorSequence = getLocationSQLData(
				marker.getTitle()).toArray(
				new CharSequence[getLocationSQLData(marker.getTitle()).size()]);

		AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
		builder.setTitle(marker.getTitle() + "'s Dates Routes & Trip Time")
				.setItems(colorSequence, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	public void startTimer(final Marker marker) {
		startTime = System.currentTimeMillis();
		Double markerlatitude = marker.getPosition().latitude;
		Double markerlongitude = marker.getPosition().longitude;
		destination = new Location("");
		destination.setLatitude(markerlatitude);
		destination.setLongitude(markerlongitude);
		locationClient.requestLocationUpdates(REQUEST, MainActivity.this);
	}

	public void cancelTimer() {
		if(destination != null || startTime != 0)
		locationClient.removeLocationUpdates(this);
		destination = null;
		startTime = 0;
		Toast.makeText(this,
				"The timer and location request have been stopped",
				Toast.LENGTH_SHORT).show();
	}

	public void setSQLData(long startTime, String destinationName,
			String routeName, long duration) {
		// StartTime Location Route duration
		SQL entry = new SQL(MainActivity.this);
		entry.open();
		entry.createEntry(startTime, destinationName, routeName, duration);
		entry.close();
	}

	public List<String> getAllSQLData() {
		SQL info = new SQL(MainActivity.this);
		info.open();
		List<String> data = info.getAllData();
		info.close();
		return data;
	}

	public List<String> getLocationSQLData(String locationName) {
		SQL info = new SQL(MainActivity.this);
		info.open();
		List<String> data = info.getlocatoinData(locationName);
		info.close();
		return data;
	}
	
//	public void updateCameraForMarker(Marker marker){
//		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
//				new LatLng(marker.getPosition().latitude, 
//						marker.getPosition().longitude-0.03), 14);
//		googleMap.animateCamera(cameraUpdate);
//	}

	@Override
	public void onLocationChanged(Location location) {
		String msg = "Distance to Location: "
				+ location.distanceTo(destination) * 1.0936 / 3
				+ " feet\nTimer is Running\nIf you are within 20 feet, "
				+ "you will arraive.";
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
				new LatLng(location.getLatitude(), location.getLongitude()),
				googleMap.getCameraPosition().zoom);
		googleMap.animateCamera(cameraUpdate);
		if ((location.distanceTo(destination) * 1.0936 / 3) < 20.0) {
			locationClient.removeLocationUpdates(this);
			arrivalTime = System.currentTimeMillis();
			setRouteName();
			Toast.makeText(this, "You have arrived!\nTimer has Stoped!",
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
		location = locationClient.getLastLocation();
		if (location != null) {
			CameraUpdate cameraUpdate = CameraUpdateFactory
					.newLatLngZoom(
							new LatLng(location.getLatitude(), location
									.getLongitude()), 10);
			googleMap.animateCamera(cameraUpdate);
		}
	}

	@Override
	public void onDisconnected() {
		Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// Do nothing
	}

}
