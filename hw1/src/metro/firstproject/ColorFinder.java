package metro.firstproject;

import metro.firstproject.R;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.Menu;
import android.view.SurfaceView;
import android.widget.NumberPicker;

public class ColorFinder extends Activity {
	
	private NumberPicker numPickerRed;
	private NumberPicker numPickerGreen;
	private NumberPicker numPickerBlue;
	private SurfaceView	surfaceView;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Start of my code
		numPickerRed = (NumberPicker) findViewById(R.id.numberPickeRed);
		numPickerRed.setMaxValue(255);
		numPickerRed.setMinValue(0);
		numPickerGreen = (NumberPicker) findViewById(R.id.numberPickerGreen);
		numPickerGreen.setMaxValue(255);
		numPickerGreen.setMinValue(0);
		numPickerBlue = (NumberPicker) findViewById(R.id.numberPickerBlue);
		numPickerBlue.setMaxValue(255);
		numPickerBlue.setMinValue(0);
		surfaceView = (SurfaceView) findViewById(R.id.surfaceView1);


		
		numPickerRed.setOnValueChangedListener(new NumberPicker.OnValueChangeListener(){
			@Override 
			public void onValueChange(NumberPicker picker, int oldVal,
					int newVal){
				surfaceView.setBackgroundColor(Color.rgb(numPickerRed.getValue(),numPickerGreen.getValue(),
						numPickerBlue.getValue()));
			}
		});
		
		numPickerGreen.setOnValueChangedListener(new NumberPicker.OnValueChangeListener(){
			@Override 
			public void onValueChange(NumberPicker picker, int oldVal,
					int newVal){
				surfaceView.setBackgroundColor(Color.rgb(numPickerRed.getValue(),numPickerGreen.getValue(),
						numPickerBlue.getValue()));
			}
		});

		numPickerBlue.setOnValueChangedListener(new NumberPicker.OnValueChangeListener(){
			@Override 
			public void onValueChange(NumberPicker picker, int oldVal,
					int newVal){
				surfaceView.setBackgroundColor(Color.rgb(numPickerRed.getValue(),numPickerGreen.getValue(),
						numPickerBlue.getValue()));
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
